#define _GNU_SOURCE

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libdebug.h"
#include "libsll.h"

int sort_test(void);

static int sort_cmp_cb_indirect(const void* a, const void* b, void* x)
{
    if((uintptr_t)x != (uintptr_t)-1) {
        P_ERR_STR("x was wrong");
        exit(EXIT_FAILURE);
    }

    uintptr_t a_d = *(const uintptr_t*)a;
    uintptr_t b_d = *(const uintptr_t*)b;
    if(a_d < b_d) return -1;
    if(a_d == b_d) return 0;
    return 1;
}

static int sort_cmp_cb_direct(const void* a, const void* b, void* x)
{
    if((uintptr_t)x != (uintptr_t)-1) {
        P_ERR_STR("x was wrong");
        exit(EXIT_FAILURE);
    }

    uintptr_t a_d = (uintptr_t)a;
    uintptr_t b_d = (uintptr_t)b;
    if(a_d < b_d) return -1;
    if(a_d == b_d) return 0;
    return 1;
}

int sort_test(void)
{
    sll_fs list;
    memset(&list, 0, sizeof(list));

    if(sll_init(&list, NULL)) {
        P_ERR_STR("init fail");
        return EXIT_FAILURE;
    }

    uintptr_t items[] = {1, 9, 8, 7, 6, 6, 5, 4, 2, 3, 5};
    const size_t num = sizeof(items)/sizeof(uintptr_t);
    for(size_t i = 0; i < num - 1; i++) {
        if(sll_push_head(&list, (void*)items[i], sizeof(uintptr_t), SLL_NOWRITE)) {
            P_ERR("Failed to push iter: %zu", i);
            return EXIT_FAILURE;
        }
    }
    sll_sort(&list, sort_cmp_cb_direct, (void*)((uintptr_t)-1));

    if(sll_insert_sorted(&list, (void*)items[num - 1], sizeof(uintptr_t), SLL_NOWRITE, sort_cmp_cb_direct, (void*)((uintptr_t)-1))) {
        P_ERR_STR("failed sorted insert");
        return EXIT_FAILURE;
    }
    
    qsort_r(items, num, sizeof(uintptr_t), sort_cmp_cb_indirect, (void*)((uintptr_t)-1));

    void* cur = NULL;
    size_t i = 0;
    while((cur = sll_pull_head(&list))) {
        if((uintptr_t)cur != items[i]) {
            P_ERR("cur: %"PRIuPTR" != item: %"PRIuPTR, (uintptr_t)cur, items[i]);
            return EXIT_FAILURE;
        }
        P_LOGIC("cur: %"PRIuPTR" == item: %"PRIuPTR, (uintptr_t)cur, items[i]);
        i++;
    }
    if(i != num) {
        P_ERR("Only had %zu items", i);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int main(void)
{
    int ret = EXIT_SUCCESS;

    if(sort_test()) {
        ret = EXIT_FAILURE;
    }

    return ret;
}
