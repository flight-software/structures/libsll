#define _GNU_SOURCE

#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "libdebug.h"
#include "libsll.h"

struct sll_sort_fs {
    int(*user_cmp)(const void*, const void*, void*);
    void* user_arg;
};

typedef struct sll_sort_fs sll_sort_fs;

/**
 * \brief Write contents of current disk to file
 * Format on disk is sizeof(size_t) bytes for the length, then the payload
 * If your structure has pointers inside, do not flush to disk.
 *
 * Size is little endian
 */

static int sll_disk_write(const sll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    if(!list->path_disk) return EXIT_SUCCESS;

    unlink(list->path_disk);
    FILE* file = fopen(list->path_disk, "w");
    if (!file) {
        P_ERR("file is null, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    sll_elem_fs* elem = list->head;
    while (elem) {
        if(fwrite(&(elem->size), 1, sizeof(elem->size), file) != sizeof(elem->size)) {
            P_ERR("Failed to write %zu bytes", sizeof(elem->size));
            fclose(file);
            return EXIT_FAILURE;
        }
        if(fwrite(elem->data, 1, elem->size, file) != elem->size) {
            P_ERR("Failed to write %zu bytes", elem->size);
            fclose(file);
            return EXIT_FAILURE;
        }
        elem = elem->next;
    }
    fclose(file);

    return EXIT_SUCCESS;
}

/**
 * \brief Read contents from disk to linked list
 */
static int sll_disk_read(sll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }
    
    if(!list->path_disk) return EXIT_SUCCESS;

    FILE* file = fopen(list->path_disk, "r");
    if (!file) {
        P_ERR("file is null, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    while (true) {
        size_t size = 0;
        if (fread(&size, 1, sizeof(size), file) != sizeof(size)) {
            break;
        }

        char* tmp_buf = malloc(size);

        if (fread(tmp_buf, 1, size, file) != size) {
            P_ERR("Failed to read %zu bytes", size);
            fclose(file);
            return EXIT_FAILURE;
        } else if (sll_push_tail(list, tmp_buf, size, SLL_NOWRITE) == EXIT_FAILURE) {
            fclose(file);
            return EXIT_FAILURE;
        }
    }
    
    fclose(file);
    return EXIT_SUCCESS;
}

/**
 * \brief Initialize a singly-linked list
 *
 * Standard SLL, except sizes of unusual properties
 *
 * - path_disk is a file backing of this structure, so we can be persistent
 *   across loads; this can be NULL
 */

int sll_init(sll_fs* list, const char* path_disk)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    memset(list, 0, sizeof(sll_fs));

    if (path_disk) {
        FILE* fp;
        if((fp = fopen(path_disk, "a+")) == NULL) {
            P_ERR("Failed to open file %s with a+ mode, errno: %d (%s)", path_disk, errno, strerror(errno));
            return EXIT_FAILURE;
        }
        fclose(fp);
        list->path_disk = strdup(path_disk);
        if (sll_disk_read(list) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Destroy a singly linked list
 *
 * List is not destroyed itself, but the pointer is passed
 * so we ensure the data it points to is up to date
 */

int sll_close(sll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    sll_clear(list);

    free(list->path_disk);

    return EXIT_SUCCESS;
}

/**
 * \brief Clear the list
 */

int sll_clear(sll_fs* list)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    }

    sll_elem_fs* cur;
    while ((cur = sll_pull_head(list)) != NULL) {
        free(cur);
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Push data to head of SLL
 */
int sll_push_head(sll_fs* list, void* data, size_t len, uint8_t flags)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    }

    sll_elem_fs* elem = malloc(sizeof(sll_elem_fs));

    if (!elem) {
        P_ERR("malloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    elem->data = data;
    elem->size = len;
    elem->next = NULL;

    if (!list->head) {
        list->head = elem;
        list->tail = elem;
    } else {
        elem->next = list->head;
        list->head = elem;
    }

    list->cur_size++;

    if (!(flags & SLL_NOWRITE)) {
        if (sll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("Error writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Push some data to the tail of the linked list
 */

int sll_push_tail(sll_fs* list, void* data, size_t len, uint8_t flags)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    }

    sll_elem_fs* elem = malloc(sizeof(sll_elem_fs));

    if (!elem) {
        P_ERR("malloc failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    elem->data = data;
    elem->size = len;
    elem->next = NULL;

    if (!list->head) {
        list->head = elem;
        list->tail = elem;
    } else {
        list->tail->next = elem;
        list->tail = list->tail->next;
    }

    list->cur_size++;

    if(!(flags & SLL_NOWRITE)) {
        if (sll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("Error writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 *  Insert node before position in list 
 */

int sll_insert(sll_fs* list, void* data, size_t len, uint8_t flags, size_t position)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    }

    sll_elem_fs* elem = malloc(sizeof(sll_elem_fs));

    elem->data = data;
    elem->size = len;
    elem->next = NULL;

    if (!list->head) {
        list->head = elem;
        list->tail = elem;
    } else {
        if (position == 0) {
            elem->next = list->head;
            list->head = elem;
        } else if (position >= list->cur_size) {
            list->tail->next = elem;
            list->tail = list->tail->next;
        } else {
            sll_elem_fs* cur = list->head;
            position--;
            while (position) {
                cur = cur->next;
                position--;
            }
            elem->next = cur->next;
            cur->next = elem;
        }
    }

    list->cur_size++;

    if (!(flags & SLL_NOWRITE)) {
        if (sll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int sll_insert_sorted(sll_fs* list, void* data, size_t len, uint8_t flags, sll_cmp_cb compare, void* arg)
{
    if(!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if(!data) {
        P_ERR_STR("data is null");
        return EXIT_FAILURE;
    }

    sll_elem_fs* elem = malloc(sizeof(sll_elem_fs));

    elem->data = data;
    elem->size = len;
    elem->next = NULL;

    if(!list->head) {
        assert(!list->tail);
        assert(list->cur_size == 0);
        list->head = elem;
        list->tail = elem;
    } else if(list->head == list->tail) {
        assert(list->cur_size == 1);
        int cmp = (compare)(list->head->data, elem->data, arg);
        if(cmp < 0) {
            // need to insert elem after head
            list->head->next = elem;
            list->tail = elem;
            list->tail->next = NULL;
        } else {
            // need to insert elem as new head
            list->head = elem;
            elem->next = list->tail;
            list->tail->next = NULL;
        }
    } else {
        assert(list->cur_size > 1);
        sll_elem_fs* prev = NULL;
        sll_elem_fs* cur = list->head;
        while(cur) {
            int cmp = (compare)(cur->data, elem->data, arg);
            if(cmp < 0) {
                // current is less than element
                prev = cur;
                cur = cur->next;
                continue;
            }
            // else, insert in between prev and cur
            break;
        }
        if(prev) {
            prev->next = elem;
        }
        elem->next = cur;
    }

    list->cur_size++;

    if(!(flags & SLL_NOWRITE)) {
        if(sll_disk_write(list) == EXIT_FAILURE) {
            P_ERR_STR("writing to disk");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Pull node from head of list
 */
void* sll_pull_head(sll_fs* list)
{
    if (!list || !list->head) {
        return NULL;
    }

    void* retval = list->head->data;
    void* old_head = list->head;
    if (list->tail == list->head) {
        list->tail = NULL;
    }
    list->head = list->head->next;

    list->cur_size--;

    free(old_head);

    sll_disk_write(list);

    return retval;
}

/**
 * \brief Pull node from tail of list
 */
void* sll_pull_tail(sll_fs* list)
{
    if (!list) {
        return NULL;
    }

    sll_elem_fs* tail_prev = list->head;

    if(!tail_prev) {
        return NULL;
    }

    while (tail_prev->next && tail_prev->next->next) {
        tail_prev = tail_prev->next;
    }

    void* retval = list->tail->data;

    list->cur_size--;

    if(list->cur_size == 0) {
        //Pulled last element, list is now empty
        free(list->head);
        list->head = NULL;
        list->tail = NULL;
    }
    else {
        //Just update tail
        list->tail = tail_prev;
        free(list->tail->next);
        list->tail->next = NULL;
    }
    
    sll_disk_write(list);

    return retval;
}

/**
 * \brief Pull any node from the list
 */

int sll_pull_ptr(sll_fs* list, const void* ptr)
{
    if (!list) {
        P_ERR_STR("list is null");
        return EXIT_FAILURE;
    } else if (!ptr) {
        P_ERR_STR("cannot remove non-existent pointer");
        return EXIT_FAILURE;
    }

    if (list->head == NULL) {
        return EXIT_FAILURE;
    } else if (ptr == list->head->data) {
        sll_pull_head(list);
        return EXIT_SUCCESS;
    } else if (list->head->next == NULL) {
        return EXIT_FAILURE;
    }

    sll_elem_fs* cur_prev = list->head;
    sll_elem_fs* cur = list->head->next;

    while (cur) {
        if (ptr == cur->data) {
            if (cur == list->tail) {
                list->tail = cur_prev;
            }
            cur_prev->next = cur->next;
            list->cur_size--;
            free(cur);

            if (sll_disk_write(list) == EXIT_FAILURE) {
                P_ERR_STR("writing to disk");
                return EXIT_FAILURE;
            }

            return EXIT_SUCCESS;
        }

        cur = cur->next;
        cur_prev = cur_prev->next;
    }

    return EXIT_FAILURE;
}

void* sll_peek_head(const sll_fs* list)
{
    if (list && list->head) {
        return list->head->data;
    }
    return NULL;
}

void* sll_peek_tail(const sll_fs* list)
{
    if (list && list->head) {
        return list->tail->data;
    }
    return NULL;
}

/**
 * \brief Search linked list with a callback
 *
 * Pretty darned useful, returs the pointer to the payload
 * Assumes providing NULL pointers for payloads is an error
 * And we return NULL if we can't find it
 */

void* sll_search(const sll_fs* list, uint8_t (*callback)(void*, void*), void* payload)
{
     if (!list || !callback) {
         return NULL;
     }

     sll_elem_fs* cur = list->head;

     while (cur) {
         if (!cur->data) {
             P_ERR_STR("We somehow managed to store a NULL payload");
             return NULL;
         }

         if (callback(cur->data, payload)) {
             return cur->data;
         }

         cur = cur->next;
     }

     return NULL;
}

static int sll_sort_cb(const void* a, const void* b, void* arg)
{
    const void* a_data = ((const sll_elem_fs*)a)->data;
    const void* b_data = ((const sll_elem_fs*)b)->data;
    sll_sort_fs* x = arg;
    return (x->user_cmp)(a_data, b_data, x->user_arg);
}

void sll_sort(sll_fs* list, sll_cmp_cb compare, void* arg)
{
    sll_elem_fs* elements = calloc(sizeof(sll_elem_fs), list->cur_size);
    
    sll_elem_fs* cur = list->head;
    sll_elem_fs* elem_cur = elements;

    while(cur) {
        *elem_cur = *cur;
        elem_cur++;
        cur = cur->next;
    }

    sll_sort_fs x = {
        .user_cmp = compare,
        .user_arg = arg,
    };

    qsort_r(elements, list->cur_size, sizeof(sll_elem_fs), sll_sort_cb, &x);

    cur = list->head;
    elem_cur = elements;

    // overwrite original sll with new order without reallocating
    while(cur) {
        sll_elem_fs* og_next = cur->next;
        *cur = *elem_cur;
        cur->next = og_next;
        elem_cur++;
        cur = og_next;
    }

    free(elements);
}

