#ifndef LIB_SLL_H
#define LIB_SLL_H

#include <stdint.h>
#include <stdlib.h>

struct sll_elem_fs {
    struct sll_elem_fs* next;
    void* data;
    size_t size;
};

#define SLL_NOWRITE    UINT8_C(1 << 1)

/*
 * push_head is for enqueueing
 * pull_tail is for dequeueing (Despite there being
 * no tail field specified).
 */

struct sll_fs {
    struct sll_elem_fs* head;
    struct sll_elem_fs* tail;
    char* path_disk;
    size_t cur_size;
};

typedef struct sll_elem_fs sll_elem_fs;
typedef struct sll_fs sll_fs;

typedef int(*sll_cmp_cb)(const void*, const void*, void*);

int sll_init(sll_fs* list, const char* path_disk);
int sll_close(sll_fs* list);
int sll_clear(sll_fs* list);

int sll_push_head(sll_fs* list, void* data, size_t len, uint8_t flags);
int sll_push_tail(sll_fs* list, void* data, size_t len, uint8_t flags);
int sll_insert(sll_fs* list, void* data, size_t len, uint8_t flags, size_t position);
int sll_insert_sorted(sll_fs* list, void* data, size_t len, uint8_t flags, sll_cmp_cb compare, void* arg);

void* sll_pull_tail(sll_fs* list);
void* sll_pull_head(sll_fs* list);

int sll_pull_ptr(sll_fs* list, const void* ptr);

void* sll_peek_head(const sll_fs* list);
void* sll_peek_tail(const sll_fs* list);

void* sll_search(const sll_fs* list, uint8_t(*callback)(void*, void*), void* payload);

void sll_sort(sll_fs* list, sll_cmp_cb compare, void* arg);

#define SLL_ITER(sll___, type___, elem___)      \
    type___ *elem___ = NULL;            \
    sll_elem_fs *sll_elem ## elem___ = sll___.head;     \
    for(;sll_elem ## elem___ != NULL && (elem___ = sll_elem ## elem___->data) != NULL; sll_elem ## elem___ = sll_elem ## elem___->next)                 

#endif
